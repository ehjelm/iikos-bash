#!/bin/bash
# loop-or-not.bash
echo 1
for f in *; do [[ -f "$f" ]] && 
 [[ $(stat -c%s "$f") -gt 200 ]] && echo "$f"; done
echo 2
find . -maxdepth 1 -type f -size +200c -print0 |
 xargs -0 ls -1
echo 3
find . -maxdepth 1 -type f -size +200c \
 -exec ls -1 {} \+
echo 4
find . -maxdepth 1 -type f -size +200c \
 -printf '%f/\n'
